const fs = require(`fs`);
const fsPath = require("path");


function createDirectory(filePath, cb) {

    fs.mkdir(filePath, { recursive: true }, (err) => {

        if (err) {

            console.log("error");

        } else {

            console.log("Directory created Sucessfully");

            cb();
        }

    });

}

function createDeleteFile(filePath, fileNum = 10) {

    for (let i = 0; i < fileNum; i++) {

        let randomName = new Date().getTime().toString() + Math.random().toFixed(2);

        let rfName = randomName + ".Json"

        fs.writeFile(fsPath.join(filePath, rfName), "hello world" + randomName, (err) => {


            if (err) {

                console.log(err);
            } else {

                console.log(` ${rfName} file created`);


                fs.unlink(fsPath.join(filePath, rfName), (err) => {

                    if (err) {

                        throw err;
                    } else {
                        console.log(` ${rfName} file deleted`);
                    }
                });
            }



        });

    }



}

module.exports.createDirectory = createDirectory;
module.exports.createDeleteFile = createDeleteFile;
