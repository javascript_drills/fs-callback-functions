
const fs = require(`fs`);

const fsPath = require(`path`);


/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/



// Q 1  : Function for Reading file lipsum.txt.
function ReadFile(fileName, cb) {

    fs.readFile(fileName, "utf8", function (err, data) {

        if (err) {
            console.error(err);
        }

        cb(data);

    });

}


// Q2 : Function to convert file data to uppercase.
function Touppercase(data, cb1) {

    let fileName2 = "uppercase_lipsum.txt";

    data = data.toUpperCase();

    fs.writeFile(fileName2, data, (error) => {

        if (error) {
            console.log(error);
        } else {

            fs.writeFile("filenames.txt", "uppercase_lipsum.txt", (error) => {

                if (error) {
                    console.error(error);
                } else {
                    console.log(`${fileName2} added to filenames.txt`);

                    cb1(data);
                }

            })
        }
    })

}



// Q3 : convert new file to lowercase - split sentences - write new file name filesname.txt
function TolowercaseSplit(data, cb2) {

    let fileName3 = "lowercase_Splitted_sentences_lipsum.txt";

    data = data.toLowerCase()
        .split(". ")
        .join(".\n");

    fs.writeFile(fileName3, data, (error) => {

        if (error) {
            console.log(error);
        } else {

            fs.appendFile("filenames.txt", "\n" + fileName3, (error) => {

                if (error) {
                    console.error(error);
                } else {
                    console.log(`${fileName3} added to filenames.txt`);

                    cb2(data);

                }

            })

        }
    });
}


// Q4 : read New files - sort the content - write it to new file - store file name to filenames.txt
function SortFile(data1, cb4) {

    let sortedFile = "sorted_lipsum.txt";

    fs.readFile("uppercase_lipsum.txt", "utf8", function (err, data) {

        if (err) {
            console.error(err);
        }

        let allData = data + data1;

        allData = allData.split(" ");

        allData.sort();

        fs.writeFile(sortedFile, allData.toString(), (err) => {

            if (err) {
                console.error(err);
            } else {

                console.log(`${sortedFile} is created`);

                fs.appendFile("filenames.txt", "\n" + sortedFile, (error) => {

                    if (error) {
                        console.error(error);
                    } else {
                        console.log(`${sortedFile} added to filenames.txt`);

                        cb4();

                    }

                })

            }
        })

    });
}


// Q5 : Read filenames.txt - delete all files present in the filenames.txt

function DeleteNewFiles() {

    fs.readFile("filenames.txt", "utf8", (err, data) => {

        if (err) {
            console.error(err);
        }

        let filesArray = data.split("\n");

        console.log(filesArray);

        filesArray.map(filename => {

            fs.unlink(filename, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log(`${filename} is deleted`);
                }
            })
        });


    });


}


module.exports.readFunction = ReadFile;

module.exports.toUppercaseFunction = Touppercase;

module.exports.toLowercaseAndSplitFunction = TolowercaseSplit;

module.exports.toSort = SortFile;

module.exports.toDeleteNewFiles = DeleteNewFiles;

