
const readFile = require(`../problem2.cjs`);

const fileName = "lipsum.txt" ;

readFile.readFunction( fileName ,(data) => {
    readFile.toUppercaseFunction(data, (data) => {
        readFile.toLowercaseAndSplitFunction(data, (data) =>{
            readFile.toSort(data , () =>{
                readFile.toDeleteNewFiles();
            })
        });
    })
});